# Lab 2

In this lab, we will use *Ballerina*'s command-line interface to create services.


## Problem 1

Choose the appropriate commands to create two projects: *greeter* and *student*. Both projects run a service and execute the following simple protocol. *greeter* sends a __hello__ message to *student*, to which it responds with student details. (student number, fulln ame, course code and designation).

## Problem 2

Create two projects, *prj1* and *prj2*. *prj1* randomly selects a number between 10 and 60 and sends it to *prj2*. The latter then computes the factors of the selected number and returns them.

