DSP620S Assignment 2 Presentation Schedule
===========================================

In this document, we compile the presentation notes for each group.

# Presentation time slots

| Date | Time slot | Group |
|------|-----------|-------|
|01/02/2021 | 09:00 - 09:20 |  |
|01/02/2021 | 09:20 - 09:40 |  |
|01/02/2021 | 09:40 - 10:00 |  |
|01/02/2021 | 10:00 - 10:20 |  |
|01/02/2021 | 10:20 - 10:40 |  |
|01/02/2021 | 10:40 - 11:00 |  |
|01/02/2021 | 11:00 - 11:20 |  |
|01/02/2021 | 11:20 - 11:40 |  |
|01/02/2021 | 11:40 - 12:00 |  |
|01/02/2021 | 12:00 - 12:20 |  |
|01/02/2021 | 12:20 - 12:40 |  |
|01/02/2021 | 12:40 - 13:00 |  |
|01/02/2021 | 17:00 - 17:20 |  |
|01/02/2021 | 17:20 - 17:40 |  |
|01/02/2021 | 17:40 - 18:00 |  |
|01/02/2021 | 18:00 - 18:20 |  |
|01/02/2021 | 18:20 - 18:40 |  |
|01/02/2021 | 18:40 - 19:00 |  |
|02/02/2021 | 09:00 - 09:20 |  |
|02/02/2021 | 09:20 - 09:40 |  |
|02/02/2021 | 09:40 - 10:00 |  |
|02/02/2021 | 10:00 - 10:20 |  |
|02/02/2021 | 10:20 - 10:40 |  |
|02/02/2021 | 10:40 - 11:00 |  |
|02/02/2021 | 11:00 - 11:20 |  |
|02/02/2021 | 11:20 - 11:40 |  |
|02/02/2021 | 11:40 - 12:00 |  |
|02/02/2021 | 12:00 - 12:20 |  |
|02/02/2021 | 12:20 - 12:40 |  |
|02/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 09:00 - 09:20 |  |
|03/02/2021 | 09:20 - 09:40 |  |
|03/02/2021 | 09:40 - 10:00 |  |
|03/02/2021 | 10:00 - 10:20 |  |
|03/02/2021 | 10:20 - 10:40 |  |
|03/02/2021 | 10:40 - 11:00 |  |
|03/02/2021 | 11:00 - 11:20 |  |
|03/02/2021 | 11:20 - 11:40 |  |
|03/02/2021 | 11:40 - 12:00 |  |
|03/02/2021 | 12:00 - 12:20 |  |
|03/02/2021 | 12:20 - 12:40 |  |
|03/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 17:00 - 17:20 |  |
|03/02/2021 | 17:20 - 17:40 |  |
|03/02/2021 | 17:40 - 18:00 |  |
|03/02/2021 | 18:00 - 18:20 |  |
|03/02/2021 | 18:20 - 18:40 |  |
|03/02/2021 | 18:40 - 19:00 |  |
|04/02/2021 | 09:00 - 09:20 |  |
|04/02/2021 | 09:20 - 09:40 |  |
|04/02/2021 | 09:40 - 10:00 |  |
|04/02/2021 | 10:00 - 10:20 |  |
|04/02/2021 | 10:20 - 10:40 |  |
|04/02/2021 | 10:40 - 11:00 |  |
|04/02/2021 | 11:00 - 11:20 |  |
|04/02/2021 | 11:20 - 11:40 |  |
|04/02/2021 | 11:40 - 12:00 |  |
|04/02/2021 | 12:00 - 12:20 |  |
|04/02/2021 | 12:20 - 12:40 | Group 4 |
|04/02/2021 | 12:40 - 13:00 | Group 3 |
|04/02/2021 | 17:00 - 17:20 |  |
|04/02/2021 | 17:20 - 17:40 |  |
|04/02/2021 | 17:40 - 18:00 | Group 6 |
|04/02/2021 | 18:00 - 18:20 |  |
|04/02/2021 | 18:20 - 18:40 |  |
|04/02/2021 | 18:40 - 19:00 | Group 2 |
|05/02/2021 | 09:00 - 09:20 |  |
|05/02/2021 | 09:20 - 09:40 |  |
|05/02/2021 | 09:40 - 10:00 |  |
|05/02/2021 | 10:00 - 10:20 |  |
|05/02/2021 | 10:20 - 10:40 |  |
|05/02/2021 | 10:40 - 11:00 |  |
|05/02/2021 | 11:00 - 11:20 |  |
|05/02/2021 | 11:20 - 11:40 |  |
|05/02/2021 | 11:40 - 12:00 |  |
|05/02/2021 | 12:00 - 12:20 |  |
|05/02/2021 | 12:20 - 12:40 |  |
|05/02/2021 | 12:40 - 13:00 |  |
|10/02/2021 | 18:00 - 18:20 | Group 1 |

# Evaluation criteria

The following criteria will be followed to assess each submission

* design **VoTo** following a micro-service architectural style (You will justify your problem decomposition into services);
* design and implement an API gateway using graphql as your API query language;
* deploy and configure Kafka for all communication in the system (services and API gateway);
* deploy the services using containerisation and orchestration (Docker and Kubernetes).


# Notes

## Group 1

- Repository: [Submission](https://github.com/Rosvita10/Assignment-2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kay-Lynne van Wyk | 219114056 |  |
| Rosvita Haikera | 214076008 |  |

- Presentation
	- Date: 10/02/2021
	- Time: 18:00 -18:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments
- Mark:

## Group 2

- Repository: [Submission](https://github.com/218118740/VoTo.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Silas T. Nghuushi | 218118740 | PT |
| Sakeus Nangolo | 219150354 | PT |


- Presentation
	- Date: 03/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments

- Mark:

## Group 3

- Repository: [Submission](https://github.com/Oprah25/DSP-assignmnt-2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Johannes Naemi N. | 214009084 | PT |
| Nghidinwa Vlatileni N.V. | 217090044 | FT|
| David Ingashipola Andreas | 217111904 | FT |
| Julius Matheus | 214082474 | FT|


- Presentation
	- Date: 04/02/2021
	- Time: 12:40 - 13:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments

- Mark:

## Group 4

- Repository: [Submission](https://github.com/oUchezuba/assignment2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Owen Uchezuba | 219038309 | FT |
| De Almeida Reis L. R. | 219081662 | FT |
| Jurema Martins | 219060789 | FT |

- Presentation
	- Date: 04/02/2021
	- Time: 12:20 - 12:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments
- Mark:

## Group 5

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-voto)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tinashe Kanengoni | 219034559 | PT |
| Antonio Reinecke | 219143218 | FT |
| Dionosius A. Beukes | 217099114 | FT |
| Lemuel Mayinoti | 219156786 | FT|

- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments
- Mark:

## Group 6

- Repository: [Submission](https://gitlab.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Jimmy Sergio Damiao | 218090013 | PT |
| Eugene Mwewa | 218040245 | FT |
| Hena Sibalatani | 219153566 | FT |
| Imelda Haufiku | 219065128 | FT|

- Presentation
	- Date: 04/02/2021
	- Time: 17:40 - 18:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- Server-side logic
			- Comments
		- deployment with Kubernetes and Docker
			- Comments
- Mark:
