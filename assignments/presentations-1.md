DSP620S Assignment 1 Presentation Schedule
===========================================

In this document, we compile the presentation notes for each group.

# Presentation time slots

| Date | Time slot | Group |
|------|-----------|-------|
|09/12/2020 | 11:15 - 11:35 | Group 17 |
|11/12/2020 | 11:00 - 11:20 | Group 24 |
| | | |
|01/02/2021 | 09:00 - 09:20 |  |
|01/02/2021 | 09:20 - 09:40 |  |
|01/02/2021 | 09:40 - 10:00 |  |
|01/02/2021 | 10:00 - 10:20 |  |
|01/02/2021 | 10:20 - 10:40 |  |
|01/02/2021 | 10:40 - 11:00 |  |
|01/02/2021 | 11:00 - 11:20 |  |
|01/02/2021 | 11:20 - 11:40 |  |
|01/02/2021 | 11:40 - 12:00 |  |
|01/02/2021 | 12:00 - 12:20 |  |
|01/02/2021 | 12:20 - 12:40 |  |
|01/02/2021 | 12:40 - 13:00 |  |
|01/02/2021 | 17:00 - 17:20 |  |
|01/02/2021 | 17:20 - 17:40 |  |
|01/02/2021 | 17:40 - 18:00 |  |
|01/02/2021 | 18:00 - 18:20 |  |
|01/02/2021 | 18:20 - 18:40 |  |
|01/02/2021 | 18:40 - 19:00 |  |
|02/02/2021 | 09:00 - 09:20 |  |
|02/02/2021 | 09:20 - 09:40 |  |
|02/02/2021 | 09:40 - 10:00 |  |
|02/02/2021 | 10:00 - 10:20 |  |
|02/02/2021 | 10:20 - 10:40 |  |
|02/02/2021 | 10:40 - 11:00 |  |
|02/02/2021 | 11:00 - 11:20 |  |
|02/02/2021 | 11:20 - 11:40 |  |
|02/02/2021 | 11:40 - 12:00 |  |
|02/02/2021 | 12:00 - 12:20 |  |
|02/02/2021 | 12:20 - 12:40 |  |
|02/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 09:00 - 09:20 |  |
|03/02/2021 | 09:20 - 09:40 |  |
|03/02/2021 | 09:40 - 10:00 |  |
|03/02/2021 | 10:00 - 10:20 |  |
|03/02/2021 | 10:20 - 10:40 |  |
|03/02/2021 | 10:40 - 11:00 | Group 4 |
|03/02/2021 | 11:00 - 11:20 | Group 14 |
|03/02/2021 | 11:20 - 11:40 |  |
|03/02/2021 | 11:40 - 12:00 |  |
|03/02/2021 | 12:00 - 12:20 |  |
|03/02/2021 | 12:20 - 12:40 |  |
|03/02/2021 | 12:40 - 13:00 |  |
|03/02/2021 | 17:00 - 17:20 |  |
|03/02/2021 | 17:20 - 17:40 |  |
|03/02/2021 | 17:40 - 18:00 |  |
|03/02/2021 | 18:00 - 18:20 | Group 8 |
|03/02/2021 | 18:20 - 18:40 |  |
|03/02/2021 | 18:40 - 19:00 | Group 12 |
|04/02/2021 | 09:00 - 09:20 |  |
|04/02/2021 | 09:20 - 09:40 |  |
|04/02/2021 | 09:40 - 10:00 | Group 19 |
|04/02/2021 | 10:00 - 10:20 |  |
|04/02/2021 | 10:20 - 10:40 |  |
|04/02/2021 | 10:40 - 11:00 |  |
|04/02/2021 | 11:00 - 11:20 |  |
|04/02/2021 | 11:20 - 11:40 |  |
|04/02/2021 | 11:40 - 12:00 |  |
|04/02/2021 | 12:00 - 12:20 | Group 13 |
|04/02/2021 | 12:20 - 12:40 |  |
|04/02/2021 | 12:40 - 13:00 |  |
|04/02/2021 | 17:00 - 17:20 | Group 10 |
|04/02/2021 | 17:20 - 17:40 | Group 9 |
|04/02/2021 | 17:40 - 18:00 |  |
|04/02/2021 | 18:00 - 18:20 |  |
|04/02/2021 | 18:20 - 18:40 | Group 25 |
|04/02/2021 | 18:40 - 19:00 | Group 11 |
|05/02/2021 | 09:00 - 09:20 | Group 5 |
|05/02/2021 | 09:20 - 09:40 |  |
|05/02/2021 | 09:40 - 10:00 |  |
|05/02/2021 | 10:00 - 10:20 | Group 18 |
|05/02/2021 | 10:20 - 10:40 |  |
|05/02/2021 | 10:40 - 11:00 |  |
|05/02/2021 | 11:00 - 11:20 |  |
|05/02/2021 | 11:20 - 11:40 |  |
|05/02/2021 | 11:40 - 12:00 |  |
|05/02/2021 | 12:00 - 12:20 |  |
|05/02/2021 | 12:20 - 12:40 |  |
|05/02/2021 | 12:40 - 13:00 |  |

# Evaluation criteria

The following criteria will be followed to assess each submission

* Definition of the remote interface in **Protocol Buffer**.
* Implementation of the gRPC client and server in the **Ballerina language**.
* Implementation of the server-side logic in response to the remote invocations, including the persistent storage and versioning.


# Notes

## Group 1

- Repository: [Submission](https://github.com/Sacky061/DSP-first-project.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Shifidi Metumo N | 219056676 | FT |
| Kudumo A. K. | 217005195 | PT |
| Leornardo Petrus | 217070418 | FT |
| Simon S. S. | 213031272 | PT |

- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 2

- Repository: [Submission](https://gitlab.com/ronaldsangunji/dsp-project)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Nestor Nathingo | 219051844 | FT |
| Sakaria Shilikomwenyo | 219055572 | FT |
| Simeon Hifikepunye | 219047987 | FT |
| Ronald Sangunji | 217069649 | FT |

- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 3

- Repository: [Submission](https://gitlab.com/andsim/dsp.assignment01)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Andrew P. Ndapito |  |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 4

- Repository: [Submission](https://gitlab.com/makosaisaac/dspfirstassignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Isaac Makosa | 219005516 | FT |
| Takunda Hwaire | 219155380 | FT |
| Tafadzwa Taringa | 219003068 | FT |

- Presentation
	- Date: 03/02/2021
	- Time: 10:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 5

- Repository: [Submission](https://github.com/gbananas190/DSP-assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Gifty Muhinda | 217109187 | FT |
| Johannes Shishasha | 217056245 | FT |
| Olaf Benjamin | 218002343 | FT |


- Presentation
	- Date: 05/02/2021
	- Time: 09:00 - 09:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 6

- Repository: [Submission](https://github.com/Ashivudhi/DSP620S-Assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Erro R. Ashivudhi | 216019362 | PT |
| David Ingashipola Andreas | 217111904 | FT |
| Joseph Nairenge | 211055832 | PT |
| Valombola Simon | 216022959 | PT |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 7

- Repository: [Submission](https://gitlab.com/dsp620s-2020-group_mmxx/dsp620s-assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Florencia Kauhanda | 214000273 | PT |
| Khoa Goodwill | 219080534 | PT |
| Ushi Mukaru | 20010475 | PT |
| Vulikeni Kashikuka | 200442368 | PT |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 8

- Repository: [Submission](https://github.com/Oprah25/DSP-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Naemi N. Johannes | 214009804 |  |
| Kay-Lynne van Wyk | 219114056 |  |
| Rosvita Haikera | 214076008 |  |


- Presentation
	- Date: 03/02/2021
	- Time: 18:00 - 18:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 9

- Repository: [Submission](https://github.com/Oprah25/DSP-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Eliakim Haufiku | 215014928 | FT |
| Mwafangeyo Germias | 218060750 | FT |
| Hamushila Petrus | 218074336 | FT |
| Nghipandulwa Leonard | 218060343 | FT |


- Presentation
	- Date: 04/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 10

- Repository: [Submission](https://github.com/Oprah25/DSP-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Jimmy Sergio Damiao | 218090013 | PT |


- Presentation
	- Date: 04/02/2021
	- Time: 17:00 - 17:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 11

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tinashe Kanengoni | 219034559 | PT |
| Antonio Reinecke | 219143218 | FT |
| Dionosius A. Beukes | 217099114 | FT |


- Presentation
	- Date: 04/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 12

- Repository: [Submission](https://github.com/218118740/Cali.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Silas T. Nghuushi | 218118740 | PT |
| Jesmine Cloete | 201032554 | PT |
| Erikson Abraham | 218062974 | PT |
| Sakeus Nangolo | 219150354 | PT |


- Presentation
	- Date: 03/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 13

- Repository: [Submission](https://github.com/218118740/Cali.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Owen Uchezuba | 219038309 | FT |
| De Almeida Reis L. R. | 219081662 | FT |
| Jurema Martins | 219060789 | FT |

- Presentation
	- Date: 04/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 14

- Repository: [Submission](https://github.com/fadedphantom/DSP_Assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Michael Apata | 219050449 |  |
| Matheus J. Tangeni | 214082474 |  |
| Nghidinwa N.V. Vatileni | 217090044 |  |
| Nghidinwa Jason P. G. Du Plessis | 217076459 |  |


- Presentation
	- Date: 03/02/2021
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 15

- Repository: [Submission](https://github.com/Selma-Auala/DSP-Assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lazarus Matheus | 219067023 |  |
| Stanza Narib | 217144810 |  |
| Selma Auala | 219009430 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:

## Group 16

- Repository: [Submission](https://github.com/WillyWonker69/DSP_ClassOf2020_ExclusiveIT)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Brave Wilson Kahweka | 219070318 | FT |
| Mino V. P. David | 219079846 | FT |
| Cesario D. C. Alves | 219150494 | FT |

- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 17

- Repository: [Submission](https://github.com/kauly-dot-repo/DSP-Assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kaulyaalalwa Peter | 219143765 | FT |
| Oletu M. Shikomba | 219060355 | FT |
| Adilson D. Da Costa | 219020124 | FT |
| Emanuel A. Q. Vuma | 219091617 | FT |


- Presentation
	- Date: 09/12/2020
	- Time: 11:15 - 11:35
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 25/11/2020
			- all members contributed: yes
		- remote interface
			- write function
			- update function
			- read with key
			- read with key and version
			- read with criteria (band name, date, ...)
			- the objects in these functions are sometimes weird
		- gRPC client
			- the client implements all functions, except the read with criteria
			- the use cases are hardcoded
		- gRPC server
			- the server implements all the functions, but does not do streaming for the read with criteria
		- Server-side logic
			- all records are persisted in a Mongodb instance
			- the version numbers are stored in the db instead of a separate implementation
- Mark:


## Group 18

- Repository: [Submission](https://github.com/VilhoHub/DSP-Assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vilho Nguno | 219068208 | FT |
| Vesta Ngomberume | 219085803 | FT |
| Vijanda V. Herunga | 216016681 | FT |
| Shania Nkusi | 219051704 | FT |


- Presentation
	- Date: 05/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 19

- Repository: [Submission](https://gitlab.com/Lee439/dsp-assignment)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lemuel Mayinoti | 219156786 | FT |
| LeeRoy Oliver | 218113951 | FT |


- Presentation
	- Date: 04/02/2021
	- Time: 09:40 - 10:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 20

- Repository: [Submission](https://gitlab.com/grouppros2020/dsp-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Helena Nambonga | 219063117 | FT |
| Hena Sibalatani | 219153566 | FT |
| Eugene Mwewa | 218040245 | FT |
| Imelda Haufiku | 219065128 | FT |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 21

- Repository: [Submission](https://github.com/ScientistFromIcarus/Assignment1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Filemon Frans | 219000131 |  |
| Emma | 219067627 |  |
| Joel Paim | 218063067 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 22

- Repository: [Submission](https://github.com/hpmouton/Distributed-Systems-assignment.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje | 219116458 |  |
| Vemuna Kaurimuje | 219048355 |  |
| Hubert Mouton | 219079714 |  |
| Maria Thomas | 219063060 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 22/11/2020
			- all members contributed: yes
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 23

- Repository: [Submission](https://gitlab.com/dsp-assignment/project-1.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Lazarus Matheus | 219067023 |  |
| Basilius Shivute | 219060754 |  |
| Selma Auala | 219009430 |  |
| Christoph Iitope | 217139485 |  |


- Presentation
	- Date:
	- Time:
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:


## Group 24

- Repository: [Submission](https://github.com/218064187/cali.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Stephen Shilongo | 218059299 | PT |
| Bill Tonata | 218099509 | PT |
| Hango Ndeyapo | 218064187 | PT |


- Presentation
	- Date: 11/12/2020
	- Time: 11:00 - 11:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 21/11/2020
			- all members contributed: no (only one)
		- remote interface
			- write function
			- update function
			- read with key
			- read with criteria (?)
			- the objects in these functions are sometimes weird
		- gRPC client
			- the client implements all functions from the proto file
			- no streaming is showing
			- the use cases are hardcoded
		- gRPC server
			- comments about the server
			- the server implements all the functions in the client, but does not do streaming for the read with criteria
		- Server-side logic
			- the records are stored in an in-memory map: no persistence of records
			- the version numbers are stored in the map, no proper implementation of the versions
- Mark:

## Group 25

- Repository: [Submission](https://gitlab.com/consuelo-ogbokor/dsp-assignment-1)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Elo Gloria Ogbokor | 216062195 | FT |

- Presentation
	- Date: 04/2/2021
	- Time: 18:20 - 18:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit:
			- all members contributed:
		- remote interface
			- list remote functions
		- gRPC client
			- comments about the client
		- gRPC server
			- comments about the server
		- Server-side logic
- Mark:
